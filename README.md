# Doumbouya-Test-IZICAP



## CHATGPT API CALLING PROJECT

The objective of this project is to set up a microservice which will be an API allowing to communicate with ChatGPT.

In my case i use Spring boot to build the API.

## HOW TO EXECUTE THIS PROJECT ?

- THE FIRST STEP IS TO :

    Build a docker image by executing this following command:

```
mvn clean package
docker build -t test_izicap:1.0 .
```

-  THE SECOND STEP IS TO:

    Run the docker image with this following command:

```
docker run -p 8000:8090 test_izicap:1.0
```

- AND THE LAST STEP IS TO USE SWAGGER TO DO THE TEST BY USING THIS LINK:

```
swagger-ui.html
```
## TOOLS AND OTHERS

- Spring Boot
- Maven
- JUnit
- Mockito
- Swagger

