package medoum.test.izicap.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import medoum.test.izicap.bean.CompletionRequest;
import medoum.test.izicap.bean.CompletionResponse;
import medoum.test.izicap.client.OpenAiApiClient;
import medoum.test.izicap.enums.Services;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ChatGptService {

    @Autowired
    private ObjectMapper jsonMapper;

    @Autowired
    private OpenAiApiClient client;

    public String chatWithGpt3(String message) throws Exception {
        var completion = CompletionRequest.defaultWith(message);
        var postBodyJson = jsonMapper.writeValueAsString(completion);
        var responseBody = client.postToOpenAiApi(postBodyJson, Services.GPT_3);
        var completionResponse = jsonMapper.readValue(responseBody, CompletionResponse.class);
        return completionResponse.firstAnswer().orElseThrow();
    }
}
