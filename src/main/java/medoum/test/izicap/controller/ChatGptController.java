package medoum.test.izicap.controller;


import com.opencsv.CSVWriter;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import medoum.test.izicap.service.ChatGptService;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.web.bind.annotation.*;

import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;

@RestController
@RequestMapping("/api")
public class ChatGptController {
    @Autowired
    private ChatGptService chatGptService;

    @PostMapping(path = "/post")
    @ApiOperation(value = "Ask a question")
    public String post(@ApiParam(name = "userInput", required = true) @RequestBody String userInput) throws Exception {
        Path customerDatabasePath = Paths.get("file.csv");
        String response = chatGptService.chatWithGpt3(userInput);
        CSVWriter writer;
        try {
            writer = new CSVWriter(new FileWriter(String.valueOf(customerDatabasePath), true));
            String[] data = {userInput, response};
            writer.writeNext(data);
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return  response;
    }
}

