package medoum.test.izicap.bean;

import java.util.List;
import java.util.Optional;

// Completion response

public record CompletionResponse(Usage usage, List<Choice> choices) {
	
	public Optional<String> firstAnswer() {
		if (choices == null || choices.isEmpty())
			return Optional.empty();
		return Optional.of(choices.get(0).text);
	}
	
	public record Usage(int total_tokens, int prompt_tokens, int completion_tokens) {}
	
	public record Choice(String text) {}
}
