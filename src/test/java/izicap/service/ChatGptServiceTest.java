package izicap.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import medoum.test.izicap.bean.CompletionResponse;
import medoum.test.izicap.client.OpenAiApiClient;
import medoum.test.izicap.service.ChatGptService;
import org.jboss.jandex.TypeTarget;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.test.context.junit4.SpringRunner;


import java.util.Collections;

import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
public class ChatGptServiceTest {


    @InjectMocks
    private ChatGptService chatGptService;

    @Mock
    private OpenAiApiClient openAiApiClient;

    @Mock
    private ObjectMapper objectMapper;

    @Test
    public void testChatWithGpt3() throws Exception {
        CompletionResponse completionResponseMock = createCompletion();

        String responseBody ="{\"id\":\"cmpl-6eCDrKopqQYSUFs4PcwEvW9lCaVRv\",\"object\":\"text_completion\",\"created\":1675038123,\"model\":\"text-davinci-003\",\"choices\":[{\"text\":\"\\n\\nStorage refers to the process of saving data and.\",\"index\":0,\"logprobs\":null,\"finish_reason\":\"stop\"}],\"usage\":{\"prompt_tokens\":4,\"completion_tokens\":88,\"total_tokens\":92}}";
        //when
        when(objectMapper.writeValueAsString(any())).thenReturn("{\"model\":\"text-davinci-003\",\"prompt\":\"what is docker\",\"temperature\":1.0,\"max_tokens\":4000}");
        when(openAiApiClient.postToOpenAiApi(any(), any())).thenReturn(responseBody);
        when(objectMapper.readValue(anyString(),eq(CompletionResponse.class))).thenReturn(completionResponseMock);
        //then
        String result = chatGptService.chatWithGpt3("what is docker");

        //verify
        Assert.assertEquals("Docker is a container platform that", result);
    }

    private CompletionResponse createCompletion(){
        CompletionResponse.Usage usage = new CompletionResponse.Usage(93, 3, 90);
        CompletionResponse.Choice choice = new CompletionResponse.Choice("Docker is a container platform that");
        CompletionResponse completionResponse = new CompletionResponse( usage, Collections.singletonList(choice));
        return completionResponse;
    }

}
